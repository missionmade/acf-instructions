=== Advanced Custom Fields Enhanced Instructions ===
Contributors: simonhughes
Tags: acf, fields, custom fields, improved instructions
Requires at least: 4.7
Tested up to: 5.7
Requires PHP: 5.6
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Add BBCode syntax to field instructions to improve readability.

== Description ==

Add BBCode markup to ACF instructions to improved readability and convey a clear message for the end user.
