<?php

/**
 * ACF Instructions
 *
 * Plugin Name: ACF Instructions
 * Plugin URI:  https://wordpress.org/plugins/acf-instructions/
 * Description: Improve the instructions message with BBCode syntax.
 * Version:     1.0
 * Author:      Simon Hughes
 * Author URI:  https://bitbucket.org/missionmade/acf-instructions/
 * License:     GPLv2 or later
 * License URI: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * Text Domain: acf-instructions
 * Requires at least: 4.9
 * Requires PHP: 5.2.4
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the GNU
 * General Public License version 2, as published by the Free Software Foundation. You may NOT assume
 * that you can use any other version of the GPL.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

add_filter('acf/prepare_field', function ($field) {
    if (!empty($field['instructions'])) {
        $field['instructions'] = acf_instructions_bbparser::parse($field['instructions']);
    }

    return $field;
}, 99, 1);


class acf_instructions_bbparser
{
    protected static $rules = [
        '\[b\](.*)\[\/b\]' => '<b>$1</b>',
        '\[i\](.*)\[\/i\]' => '<em>$1</em>',
        '\[u\](.*)\[\/u\]' => '<u>$1</u>',
        '\[url\](.*)\[\/url\]' => '<a href="$1">$1</a>',
        '\[url=(.+)\](.*)\[\/url\]' => '<a href="$1">$2</a>',
    ];

    public static function parse($str)
    {
        foreach (self::$rules as $find => $replace) {
            $str = preg_replace('#' . $find . '#', $replace, $str);
        }

        return $str;
    }
}
